//
// Created by hugo on 05.01.22.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include "cli.h"
#include "config.h"
#include "chronos.h"

void cli::run() {
    std::string line;
    chronos chronos;
    std::cout << "Adjugate matrix solver CLI v1.0.0" << std::endl;
    while(true) {
        std::vector<std::string> args;
        std::getline(std::cin, line);

        if(std::cin.eof()) break;

        std::istringstream iss(line);
        std::string word;

        std::string command;
        std::string path;
        int word_idx = 0;
        while (std::getline(iss, word, ' ')) {
            if(word_idx == 0) command = word;
            else {
                if(word.find("-path") != std::string::npos) {
                    path = parse_path(word);
                }

                if(word.find("-m") != std::string::npos) {
                    setMultiThread(true);
                }
            }
            word_idx++;
        }

        chronos.start();
        if(command == "determinant") {
            std::cout << "Command is: " << command << std::endl;
            execute_determinant(path);
        } else if (command == "adjugate") {
            std::cout << "Command is: " << command << std::endl;
            execute_adjugate(path);
        }
        else if(command.empty()) {
            continue;
        }
        else {
            std::cout << "Unknown command: " << command << std::endl;
            continue;
        }
        chronos.stop();
        std::cout << "Command finished." << std::endl;
        if(!path.empty())
            std::cout << "Execution time was " << chronos.get_time_in_ms() << "ms" << std::endl;

        setMultiThread(false);
    }
}

std::string cli::parse_path(std::string arg) {
    std::istringstream iss(arg);
    std::string word;
    std::string path;
    int word_idx = 0;

    while (std::getline(iss, word, '=')) {
        if(word_idx == 1) path = word;
        word_idx++;
    }

    return path;
}

void cli::execute_determinant(const std::string& path) {
    if(path.empty()) {
        matrix<long double> m = load_matrix_from_stdin();
        std::cout << "Determinant of this matrix is: " << m.determinant() << std::endl;
    }
    else {
        try {
            matrix<long double> m = load_matrix_from_file(path);
            std::cout << "Determinant of this matrix is: " << m.determinant() << std::endl;
        }
        catch (std::string e) {
            std::cout << e << std::endl;
        }
    }
}

matrix<long double> cli::load_matrix_from_stdin() {
    int dim;
    int idx = 0;

    std::cout << "Enter matrix dimension:";
    std::cin >> dim;
    std::cout << "Matrix dimension: " << dim << "x" << dim << std::endl;

    std::cout << "Enter matrix" << std::endl;
    auto * numbers = new long double[dim * dim];
    while(idx < dim * dim) {
        std::cin >> numbers[idx];
        ++idx;
    }

    matrix<long double> rv(dim, dim, numbers);
    delete [] numbers;
    return rv;
}

matrix<long double> cli::load_matrix_from_file(std::string path) {
    std::ifstream input(path, std::ios_base::in);

    if(input.fail()) {
        throw "File with the path " + path + " not found.";
    }

    int dim = 0;
    int idx = 0;

    input >> dim;
    std::cout << "Detected dimension: " << dim << "x" << dim << std::endl;
    auto * numbers = new long double[dim * dim];

    while(idx < dim * dim) {
        if(input.eof()) {
            delete [] numbers;
            throw "File has too few numbers.";
        }
        input >> numbers[idx];
        ++idx;
    }

    matrix<long double> rv(dim, dim, numbers);
    delete [] numbers;
    return rv;
}

void cli::execute_adjugate(const std::string &path) {

    if(path.empty()) {
        matrix<long double> m = load_matrix_from_stdin();
        std::cout << "Adjugate of this matrix is: " << std::endl;
        m.adjugate().print();
    }
    else {
        try {
            matrix<long double> m = load_matrix_from_file(path);
            std::cout << "Adjugate of this matrix is: " << std::endl;
            m.adjugate().print();
        }
        catch (std::string e) {
            std::cout << e << std::endl;
        }
    }
}
