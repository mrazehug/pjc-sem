//
// Created by hugo on 08.01.22.
//

#ifndef SEM_CONFIG_H
#define SEM_CONFIG_H

static bool multi_thread = false;

void setMultiThread(bool val);
bool isMultiThread();

#endif //SEM_CONFIG_H
