# Semestrální práce z PJC

V mojí semestrální práci jsem řešil problém adjungované matice.

## Komiplace, spuštění programu

Program lze zkompilovat pomocí nástroje CMake. Pro spuštění nevyžaduje žádné parametry, s programem se pak pracuje pomocí CLI.

## CLI

Po spuštění se s programem pracuje přes jeho CLI. Program má dva příkazy:

### Adjugate

**Synopsis:** adjugate [options]

Spustí příkaz pro výpočet adjungované matice. Pokud příkazu nejsou zadané žádné parametry, wizard provede uživatelem zadáním matice.

### Options

-path Cesta k souboru, který obsahuje matici, pro kterou chceme adjungovanou matici spočítat. Soubor má následující strukturu: První číslo n označuje rozměr matice. Následuje n * n čísel, které matici tvoří. 

-m Příznak, který když je nastaven, tak výpočet probíhá vícevláknově.

### Příklady

Příklad s wizardem

```shell
adjugate
Command is: adjugate
Enter matrix dimension: 3
Matrix dimension: 3x3
Enter matrix
1 2 3
4 5 6
7 8 9
Adjugate of this matrix is: 
-3 6 -3 
6 -12 6 
-3 6 -3 
```

Příklad s přepínačem path

```shell
adjugate -path=../testdata/input/input_SMALL_3
Command is: adjugate
Detected dimension: 3x3
Adjugate of this matrix is: 
52 8 -28 
-20 -16 14 
-50 2 14 
```

### Determinant

**Synopsis:** determinant [options]

Spustí příkaz pro výpočet determinantu matice. Pokud příkazu nejsou zadané žádné parametry, wizard provede uživatelem zadáním matice.

### Options

-path Cesta k souboru, který obsahuje matici, pro kterou chceme determinant spočítat. Soubor má následující strukturu: První číslo n označuje rozměr matice. Následuje n * n čísel, které matici tvoří.

-m Příznak, který když je nastaven, tak výpočet probíhá vícevláknově.

### Příklady

Příklad s wizardem

```shell
Command is: determinant
Enter matrix dimension:3
Matrix dimension: 3x3
Enter matrix
1 2 3
4 5 6
7 8 9
Determinant of this matrix is: -0
```

Příklad s přepínačem path

```shell
determinant -path=../testdata/input/input_SMALL_3
Command is: determinant
Detected dimension: 3x3
Determinant of this matrix is: -84
```

# Testování

Testoval jsem rychlost programu pro běh v jednom vlákně i ve vícero (v mém případě ve 4).
Při výpočtu paralelizuji výpočty jednotlivých determinantů.
Pro každý rozměr matice jsem provedl jak pro jednovláknový, tak pro multivláknový běh 10 měření, které jsem následně zprůměroval.
Měřil jsem pro matice o rozměrech 20x20 - 100x100. Matice menší než 20x20 řeší program moc rychle na to, aby to šlo měřit.
<table>
  <tr>
    <th>Rozměr matice</th>
    <th>Běh v jednom vlákně (ms)</th>
    <th>Běh ve vícero vláknech (ms)</th>
  </tr>
  <tr>
    <td>20x20</td>
    <td>73.2</td>
    <td>57.1</td>
  </tr>
  <tr>
    <td>30x30</td>
    <td>403.4</td>
    <td>321.6</td>
  </tr>
  <tr>
    <td>40x40</td>
    <td>1522.4</td>
    <td>853.5</td>
  </tr>
  <tr>
    <td>50x50</td>
    <td>4340.6</td>
    <td>2484.5</td>
  </tr>
  <tr>
    <td>60x60</td>
    <td>11917</td>
    <td>6243.8</td>
  </tr>
  <tr>
    <td>70x70</td>
    <td>27422.2</td>
    <td>11989.7</td>
  </tr>
  <tr>
    <td>80x80</td>
    <td>57177.8</td>
    <td>25493.5</td>
  </tr>
  <tr>
    <td>90x90</td>
    <td>76280.9</td>
    <td>39183.9</td>
  </tr>
  <tr>
    <td>100x100</td>
    <td>DNF</td>
    <td>71258.8</td>
  </tr>
</table> 

Jak je na výsledcích vidět, vícevláknové řešení skutečně program zrychluje. Při menších maticích
není zrychlení tak markantní, což je pravděpodobně způsobené častým přepínáním mezi vlákny, nicméně na velkých maticích
je čas vícevláknový zhruba poloviční, než jednovláknový.

## Poznámka k rychlosti

Na první pohled by se mohlo zdát, že má implementace je pomalá, už při výpočtu adjungované matice 50x50 se program zamyslí. 
Není to ale pomalou implementací, ale tím, že pro výpočet matice nxn musí program počítat n * n * (determinant matice o rozměrech n - 1).
Mohla by vyvstat otázka, jestli neexistuje lepší algoritmus pro hledání adjungované matice, avšak žádnou lepší implementaci jsem nenašel.
