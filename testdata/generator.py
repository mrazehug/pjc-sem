import numpy as np
import random

def generate_determinant_test(size):
    input_det_file_name = "input_"
    output_det_file_name = "output_det_"
    output_adj_file_name = "output_adj_"
    if size < 10:
        input_det_file_name += "SMALL"
        output_det_file_name += "SMALL"
        output_adj_file_name += "SMALL"

    elif size < 80:
        input_det_file_name += "MEDIUM"
        output_det_file_name += "MEDIUM"
        output_adj_file_name += "MEDIUM"

    else:
        input_det_file_name += "LARGE"
        output_det_file_name += "LARGE"
        output_adj_file_name += "LARGE"

    input_det_file_name += "_" + str(size)
    output_det_file_name += "_" + str(size)
    output_adj_file_name += "_" + str(size)

    numbers = []

    input_file = open("input/" + input_det_file_name, "w")
    output_file = open("output/" + output_det_file_name, "w")
    output_adj_file = open("output/" + output_adj_file_name, "w")
    input_file.write(str(size) + "\n")
    for i in range(0, size):
        tmp = []
        for j in range(0, size):
            number = random.randint(1, 10)
            input_file.write(str(number))
            tmp.append(number)
            if j is not size - 1:
                input_file.write(" ")
        if i is not size - 1:
            input_file.write("\n")
        numbers.append(tmp)

    nparr = np.array(numbers)
    det = np.linalg.det(nparr)
    output_file.write(str(det))
    output_adj_file.write(str(np.linalg.det(nparr) * np.linalg.inv(nparr)))
    print("yeet")




if __name__ == "__main__":
    generate_determinant_test(20)
    generate_determinant_test(30)
    generate_determinant_test(40)
    generate_determinant_test(60)
    generate_determinant_test(70)
    generate_determinant_test(80)
    generate_determinant_test(90)
    # generate_determinant_test(3)
    # generate_determinant_test(4)
    # generate_determinant_test(7)
    # generate_determinant_test(8)
    # generate_determinant_test(50)
    # generate_determinant_test(60)
    # generate_determinant_test(90)
    # generate_determinant_test(100)