//
// Created by hugo on 05.01.22.
//

#pragma once

#include <iostream>
#include <cstring>
#include <math.h>
#include <mutex>
#include <thread>
#include <vector>
#include "config.h"

template <typename T>
class matrix {
private:
    int width, height;
    T * numbers;
    std::mutex * number_mutex;
    void execute_adjugate_multi(int i, int j, matrix * mat);
    matrix adjugate_multithreaded();
    matrix adjugate_singlethreaded();
public:
    matrix(int width, int height);
    matrix(int width, int height, T numbers[]);
    ~matrix();
    T operator [](int i) const    {return numbers[i];}
    T& operator [](int i) {return numbers[i];}
    T determinant();
    matrix adjugate();
    matrix transpose();
    void print();
    matrix<T> get_sub_matrix(int except_i, int except_j);
};

template<typename T>
matrix<T>::matrix(int width, int height) {
    this->width = width;
    this->height = height;
    number_mutex = new std::mutex();
    numbers = new T[width * height];
    for(int i = 0; i < width * height; ++i) {
        numbers[i] = (T)0.0;
    }
}

template<typename T>
matrix<T>::matrix(int width, int height, T values[]) {
    this->width = width;
    this->height = height;
    number_mutex = new std::mutex();
    numbers = new T[width * height];
    for(int i = 0; i < height; ++i) {
        for(int j = 0; j < width; ++j) {
            numbers[i * width + j] = values[i * width + j];
        }
    }
}

template<typename T>
T matrix<T>::determinant() {
    matrix a = matrix<T>(width, height, numbers);

    int i, j, k, offset;
    bool sign = false;
    double ratio;

    for (i = 0; i < width; i++)
    {
        if (a[i * width + i] == 0)
        {
            offset = 1;
            while ((i + offset) < width && a[(i + offset) * width + i] == 0) ++offset;
            if ((i + offset) == width) break;

            // Toggle determinant sign
            sign = !sign;
            for (j = i, k = 0; k < width; k++)
                std::swap(a[j * width + k], a[(j + offset) * width + k]);
        }

        for (j = 0; j < width; j++) {
            if (i != j) {
                ratio = a[j * width + i] / a[i * width + i];
                for (k = 0; k < width; k++) {
                    a[j * width + k] = a[j * width + k] - (a[i * width + k]) * ratio;
                }
            }
        }
    }

    T det = 1;
    for(i = 0; i < width; ++i) {
        det *= a[i * width + i];
    }

    return !sign ? det : -det;
}

template<typename T>
void matrix<T>::print() {
    for(int i = 0; i < height; ++i) {
        for(int j = 0; j < width; ++j) {
            std::cout << numbers[i * width + j] << " ";
        }
        std::cout << std::endl;
    }
}

template <typename T>
void matrix<T>::execute_adjugate_multi(int i, int j, matrix<T> * mat) {
    matrix<T> sub = get_sub_matrix(i, j);
    T det = pow(-1, i + j) * sub.determinant();
    std::lock_guard<std::mutex> guard(*number_mutex);
    (*mat)[j * width + i] = det;
}

template<typename T>
matrix<T> matrix<T>::adjugate() {
    if(isMultiThread()) {
        std::cout << "Detected multithread" << std::endl;
    }
    if(isMultiThread() && std::thread::hardware_concurrency() > 1) {
        return adjugate_multithreaded();
    }
    else {
        return adjugate_singlethreaded();
    }
}

template<typename T>
matrix<T> matrix<T>::transpose() {
    matrix rv(height, width);
    for(int n = 0; n < width * height; n++) {
        int i = n / height;
        int j = n % height;
        rv[n] = numbers[width * j + i];
    }
    return rv;
}

template<typename T>
matrix<T>::~matrix() {
    delete [] numbers;
    delete number_mutex;
}

template<typename T>
matrix<T> matrix<T>::get_sub_matrix(int except_i, int except_j) {
    matrix<T> rv(width - 1, height - 1);
    int write_index = 0;
    for (int i = 0; i < width; ++i) {
        for (int j = 0; j < height; ++j) {
            if (i != except_i && j != except_j) {
                rv[write_index++] = numbers[i * width + j];
            }
        }
    }

    return rv;
}

template<typename T>
matrix<T> matrix<T>::adjugate_multithreaded() {
    matrix<T> rv(width, height);
    std::vector<std::thread> threads;
    for(int i = 0; i < width; ++i) {
        for(int j = 0; j < height; ++j) {
            threads.push_back(std::thread(&matrix::execute_adjugate_multi, this, i, j, &rv));

            // When number of threads reaches limit of HW threads, joining happens
            if(threads.size() == std::thread::hardware_concurrency()) {
                auto it = threads.begin();
                // Looping until one of the threads is not joinable
                while(it != threads.end()) {
                    if(it->joinable()) {
                        it->join();
                        threads.erase(it);
                        break;
                    }
                    if(it == threads.end())
                        it = threads.begin();
                }
            }
        }
    }

    for(auto & thread: threads) {
        thread.join();
    }

    return rv;
}

template<typename T>
matrix<T> matrix<T>::adjugate_singlethreaded() {
    matrix<T> rv(width, height);
    std::vector<std::thread> threads;
    for(int i = 0; i < width; ++i) {
        for(int j = 0; j < height; ++j) {
            matrix<T> sub = get_sub_matrix(i, j);
            T det = pow(-1, i + j) * sub.determinant();
            std::lock_guard<std::mutex> guard(*number_mutex);
            rv[j * width + i] = det;
        }
    }

    for(auto & thread: threads) {
        thread.join();
    }

    return rv;
}





