
#include <chrono>

#pragma once

// The keeper of the time
class chronos {
private:
    std::chrono::high_resolution_clock::time_point start_time;
    std::chrono::high_resolution_clock::time_point stop_time;
public:
    void start();
    void stop();
    long get_time_in_ms();
};


