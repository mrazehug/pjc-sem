#include "cli.h"

int main(int argc, char * argv[]) {
    if(argc > 2) {
        std::cout << "Too many arguments" << std::endl;
        return 1;
    } else {
        if(argc == 2 && strcmp(argv[1], "--help") == 0) {
            std::cout << "Please refer to README.md of the repository for instructions" << std::endl;
            return 0;
        }
        else if(argc == 2) {
            std::cout << "Unknown argument: " << argv[1] << std::endl;
            return 1;
        }
    }

    cli cli;
    cli.run();
    return 0;
}
