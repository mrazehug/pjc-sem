//
// Created by hugo on 05.01.22.
//

#pragma once

#include "matrix.h"

#define MAX_LINE_LEN 512

class cli {
private:
    std::string parse_path(std::string arg);
    matrix<long double> load_matrix_from_stdin();
    matrix<long double> load_matrix_from_file(std::string path);
    void execute_determinant(const std::string& path);
    void execute_adjugate(const std::string& path);
public:
    void run();
};


