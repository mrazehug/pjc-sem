//
// Created by hugo on 08.01.22.
//
#include "config.h"


void setMultiThread(bool val) {
    multi_thread = val;
}

bool isMultiThread() {
    return multi_thread;
}

