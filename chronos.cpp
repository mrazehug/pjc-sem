//
// Created by hugo on 08.01.22.
//

#include <iostream>
#include "chronos.h"

void chronos::start() {
    start_time = std::chrono::high_resolution_clock::now();
}

void chronos::stop() {
    stop_time = std::chrono::high_resolution_clock::now();
}

long chronos::get_time_in_ms() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time).count();
}


